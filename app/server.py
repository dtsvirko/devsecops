from http.server import HTTPServer, CGIHTTPRequestHandler

handler = CGIHTTPRequestHandler
handler.cgi_directories = ['/cgi_bin']

server_address = ("", 8000)
httpd = HTTPServer(server_address, CGIHTTPRequestHandler)
httpd.serve_forever()

