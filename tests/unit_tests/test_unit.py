from app.cgi_bin._wall import Wall
import pytest
import os


@pytest.fixture(scope='module', autouse=True)
def wall_object():
    wall = Wall(USERS='tests/unit_tests/users.json', WALL='tests/unit_tests/wall.json', COOKIES='tests/unit_tests/cookies.json')
    f = open('tests/unit_tests/users.json', 'w')
    f.write('{"dtsvirko": "Qwe12345!"}')
    f.close()
    yield wall
    for i in ['tests/unit_tests/users.json', 'tests/unit_tests/wall.json', 'tests/unit_tests/cookies.json']:
        os.remove(i)


def test_find_user(wall_object):
    assert wall_object.find('dtsvirko') is True, 'Существующий пользователь не найден.'


def test_find_non_existent_user(wall_object):
    assert wall_object.find('RTYKGHT') is False, 'Найден несуществующий пользователь.'
