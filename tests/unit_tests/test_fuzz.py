from html.parser import HTMLParser
from pythonfuzz.main import PythonFuzz


@PythonFuzz
def fuzz(buf):
    try:
        string = buf.decode("ascii")
        # print(string)
        parser = HTMLParser()
        parser.feed(string)
    # except UnicodeDecodeError:
    #     pass
    except NotImplementedError:
        pass


if __name__ == '__main__':
    fuzz()