from selenium import webdriver
import pytest
from cnfg import users_file
import json
import time


@pytest.fixture(scope='session', autouse=True)
def start_driver():
    """ Select configuration depends on browser and host """
    options = webdriver.ChromeOptions()
    options.add_argument('ignore-certificate-errors')
    driver = webdriver.Chrome(desired_capabilities=options.to_capabilities())
    yield driver
    driver.quit()


# @pytest.fixture(scope='session', autouse=True)
# def start_driver():
#     options = webdriver.ChromeOptions()
#     options.add_argument('ignore-certificate-errors')
#     # options.add_argument('headless')
#     driver = webdriver.Chrome(options=options)
#     driver.maximize_window()
#     yield driver
#     driver.quit()


@pytest.fixture(scope='function', autouse=False)
def delete_new_user():
    yield
    pass


@pytest.fixture(scope='function', autouse=False)
def del_new_user():
    yield
    with open(users_file, 'r', encoding='utf-8') as f:
        users = json.load(f)
    users.pop('NewUser')
    f = open(users_file, 'w')
    f.write(json.dumps(users))
    f.close()