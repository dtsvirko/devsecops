import pytest
import json
from cnfg import users_file
import time


@pytest.fixture(scope='module', autouse=True)
def open_page(start_driver):
    start_driver.get('http://127.0.0.1:8000/cgi_bin/wall.py')


def test_title_text(start_driver):
    title_text = start_driver.title
    assert title_text == 'Стена', 'Заголовок страницы не соответствует ожидаемому.'


def test_new_user(start_driver, del_new_user):
    login_field = start_driver.find_element_by_name('login')
    login_field.send_keys('NewUser')
    password_field = start_driver.find_element_by_name('password')
    password_field.send_keys('Qwe12345678')
    new_user_submit_button = start_driver.find_element_by_id('user_submit_button')
    new_user_submit_button.click()
    with open(users_file, 'r', encoding='utf-8') as f:
        users = json.load(f)
    assert users['NewUser'] == "Qwe12345678", 'Пользователь не сохранился.'
