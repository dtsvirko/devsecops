image: python:3.7.6-alpine

stages:
  - unit_tests
  - sast_tests
  - dependency_tests
  - property_based_tests_with_hypothesis
  - fuzzing_with_pythonfuzz
  - docker_image_tests
  - dast_with_owasp_zap
  - page_speed_with_lighthouse
  - waf_tests_with_gowaftest
  - deploy

unit_tests:
  stage: unit_tests
  before_script:
    - pip3 install -r requirements.txt
  script:
    - python -m pytest --html=tests/reports/UNIT.html --self-contained-html tests/unit_tests/test_unit.py
  artifacts:
    when: always
    paths:
      - tests/reports/UNIT.html
    expire_in: 7 days


sast_tests_with_bandit:
  stage: sast_tests
  allow_failure: true
  before_script:
    - pip3 install -r requirements.txt
  script:
    - bandit -r files_with_vuln/ -f html -o tests/reports/bandit_report.html
  artifacts:
    when: always
    paths:
      - tests/reports/bandit_report.html
    expire_in: 7 days


dependency_tests_with_safety:
  stage: dependency_tests
  allow_failure: true
  before_script:
    - pip3 install -r requirements.txt
  script:
    - safety check -r requirements_with_vuln.txt --full-report > tests/reports/dependency_report.txt
  artifacts:
    when: always
    paths:
      - tests/reports/dependency_report.txt
    expire_in: 7 days


property_based_tests_with_hypothesis:
  stage: property_based_tests_with_hypothesis
  allow_failure: true
  before_script:
    - pip3 install hypothesis
    - pip3 install pytest
    - pip3 install pytest-html
  script:
    - python -m pytest --html=tests/reports/hypothesis_tests.html --self-contained-html tests/unit_tests/test_hpt.py
  artifacts:
    when: always
    paths:
      - tests/reports/hypothesis_tests.html
    expire_in: 7 days


fuzzing_with_pythonfuzz:
  stage: fuzzing_with_pythonfuzz
  allow_failure: true
  image: dtsvirko/pythonfuzz
  script:
    - python3 tests/unit_tests/test_fuzz.py > tests/reports/fuzzing_report.txt
  artifacts:
    when: always
    paths:
      - tests/reports/fuzzing_report.txt
    expire_in: 7 days


docker_image_tests:
  stage: docker_image_tests
  image: dtsvirko/trivy
  allow_failure: true
  script:
    - cd tests
    - trivy image --format template --template "@contrib/html.tpl" -o reports/docker_report.html bkimminich/juice-shop
  after_script:
    - cd ..
  artifacts:
    when: always
    paths:
      - tests/reports/docker_report.html
    expire_in: 7 days


owasp_zap_tests:
  image: owasp/zap2docker-stable
  stage: dast_with_owasp_zap
  allow_failure: true
  script:
    - mkdir /zap/wrk
    - zap-baseline.py -t https://google.com/ -j -a -r owasp_zap_report.html
  when: manual
  after_script:
    - cp /zap/wrk/owasp_zap_report.html tests/reports
    - ls -a tests/reports
  artifacts:
    when: always
    paths:
      - tests/reports/owasp_zap_report.html
    expire_in: 7 days


page_speed_with_lighthouse:
  stage: page_speed_with_lighthouse
  image: docker:19.03.1
  services:
    - docker:dind
  allow_failure: true
  script:
    - chmod 777 tests/reports
    - docker run -v $(pwd)/tests/reports:/home/chrome/reports femtopixel/google-lighthouse http://www.google.com --output html --output-path lighthouse_report.html
  artifacts:
    when: always
    paths:
      - tests/reports/lighthouse_report.html
    expire_in: 7 days


waf_tests_with_gowaftest:
  stage: waf_tests_with_gowaftest
  image: docker:19.03.1
  services:
    - docker:dind
  allow_failure: true
  before_script:
    - docker run -p 8081:80 -d -e PARANOIA=1 --rm  owasp/modsecurity-crs:3.3-apache
  script:
    - docker run -v $(pwd)/tests/reports:/app/reports --network="host" wallarm/gotestwaf --url=http://127.0.0.1:8081/ --verbose
  when: manual
  artifacts:
    when: always
    paths:
      - tests/reports/waf-evaluation-report-generic.pdf
    expire_in: 7 days


pages:
  stage: deploy
  dependencies:
    - sast_tests_with_bandit
    - unit_tests
    - dependency_tests_with_safety
    - property_based_tests_with_hypothesis
    - fuzzing_with_pythonfuzz
    - docker_image_tests
    - owasp_zap_tests
    - page_speed_with_lighthouse
    - waf_tests_with_gowaftest
  script:
    - mkdir public
    - cp -r tests/reports/* public
  artifacts:
    paths:
      - public
  rules:
    - when: always
